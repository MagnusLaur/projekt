package Calculator;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Calculator {

	private JFrame frame;
	private JTextField textField;

	double firstnumber;
	double secondnumber;
	double result;
	String operations;
	String answer;

	// Launch the application.

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculator window = new Calculator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Create the application.

	public Calculator() {
		initialize();
	}

	// Initialize the contents of the frame.

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 269, 394);
		frame.setTitle("Kalkulaator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setBackground(Color.WHITE);
		frame.getContentPane().setLayout(null);

		// Creates the contents of the text field.

		textField = new JTextField();
		textField.setBackground(Color.WHITE);
		textField.setFont(new Font("Tahoma", Font.PLAIN, 19));
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setBounds(10, 11, 232, 36);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		// Creating the buttons for the calculators

		JButton btn7 = new JButton("7");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn7.getText();
				textField.setText(EnterNumber);
			}
		});
		btn7.setBackground(Color.WHITE);
		btn7.setBounds(0, 59, 63, 59);
		btn7.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn7);

		JButton btn8 = new JButton("8");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn8.getText();
				textField.setText(EnterNumber);
			}
		});
		btn8.setBackground(Color.WHITE);
		btn8.setBounds(63, 59, 63, 59);
		btn8.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn8);

		JButton btn9 = new JButton("9");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn9.getText();
				textField.setText(EnterNumber);
			}
		});
		btn9.setBackground(Color.WHITE);
		btn9.setBounds(126, 59, 63, 59);
		btn9.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn9);

		JButton btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnumber = Double.parseDouble(textField.getText());
				textField.setText("");
				operations = "+";
			}
		});
		btnPlus.setBackground(Color.WHITE);
		btnPlus.setBounds(189, 59, 63, 59);
		btnPlus.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnPlus);

		JButton btn4 = new JButton("4");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn4.getText();
				textField.setText(EnterNumber);
			}
		});
		btn4.setBackground(Color.WHITE);
		btn4.setBounds(0, 118, 63, 59);
		btn4.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn4);

		JButton btn5 = new JButton("5");
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn5.getText();
				textField.setText(EnterNumber);
			}
		});
		btn5.setBackground(Color.WHITE);
		btn5.setBounds(63, 118, 63, 59);
		btn5.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn5);

		JButton btn6 = new JButton("6");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn6.getText();
				textField.setText(EnterNumber);
			}
		});
		btn6.setBackground(Color.WHITE);
		btn6.setBounds(126, 118, 63, 59);
		btn6.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn6);

		JButton btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnumber = Double.parseDouble(textField.getText());
				textField.setText("");
				operations = "-";
			}
		});
		btnMinus.setBackground(Color.WHITE);
		btnMinus.setBounds(189, 118, 63, 59);
		btnMinus.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnMinus);

		JButton btn1 = new JButton("1");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn1.getText();
				textField.setText(EnterNumber);
			}
		});
		btn1.setBackground(Color.WHITE);
		btn1.setBounds(0, 177, 63, 59);
		btn1.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn1);

		JButton btn2 = new JButton("2");
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn2.getText();
				textField.setText(EnterNumber);
			}
		});
		btn2.setBackground(Color.WHITE);
		btn2.setBounds(63, 177, 63, 59);
		btn2.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn2);

		JButton btn3 = new JButton("3");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn3.getText();
				textField.setText(EnterNumber);
			}
		});
		btn3.setBackground(Color.WHITE);
		btn3.setBounds(126, 177, 63, 59);
		btn3.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn3);

		JButton btnMultiply = new JButton("*");
		btnMultiply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnumber = Double.parseDouble(textField.getText());
				textField.setText("");
				operations = "*";
			}
		});
		btnMultiply.setBackground(Color.WHITE);
		btnMultiply.setBounds(189, 177, 63, 59);
		btnMultiply.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnMultiply);

		JButton btnClear = new JButton("C");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText(null);
			}
		});
		btnClear.setBackground(Color.WHITE);
		btnClear.setBounds(0, 236, 63, 59);
		btnClear.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnClear);

		JButton btn0 = new JButton("0");
		btn0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btn0.getText();
				textField.setText(EnterNumber);
			}
		});
		btn0.setBackground(Color.WHITE);
		btn0.setBounds(63, 236, 63, 59);
		btn0.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btn0);

		JButton btnPoint = new JButton(".");
		btnPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String EnterNumber = textField.getText() + btnPoint.getText();
				textField.setText(EnterNumber);
			}
		});
		btnPoint.setBackground(Color.WHITE);
		btnPoint.setBounds(126, 236, 63, 59);
		btnPoint.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnPoint);

		JButton btnDivide = new JButton("/");
		btnDivide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				firstnumber = Double.parseDouble(textField.getText());
				textField.setText("");
				operations = "/";
			}
		});
		btnDivide.setBackground(Color.WHITE);
		btnDivide.setBounds(189, 236, 63, 59);
		btnDivide.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnDivide);

		JButton btnValue = new JButton("=");
		btnValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String answer;
				secondnumber = Double.parseDouble(textField.getText());
				if (operations == "+") {
					result = firstnumber + secondnumber;
					answer = String.format("%.2f", result);
					textField.setText(answer);
				} else if (operations == "-") {
					result = firstnumber - secondnumber;
					answer = String.format("%.2f", result);
					textField.setText(answer);
				} else if (operations == "*") {
					result = firstnumber * secondnumber;
					answer = String.format("%.2f", result);
					textField.setText(answer);
				} else if (operations == "/") {
					result = firstnumber / secondnumber;
					answer = String.format("%.2f", result);
					textField.setText(answer);
				}
			}
		});
		btnValue.setBackground(Color.WHITE);
		btnValue.setBounds(0, 295, 252, 59);
		btnValue.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(btnValue);

	}
}
